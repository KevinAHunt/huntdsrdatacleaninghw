import os, sys, re

def combinefiles(newfilename):
    open(newfilename, 'w').close()
    fout=open(newfilename, "a")
    dirname, pyname = os.path.split(os.path.abspath(sys.argv[0]))

    for num in range(1,10):
        f = open(dirname + "/" + str(num) + " LAYOUT B CSV.csv")
        for line in f:
            fout.write(line)

    fout.close()

def getPath(fromFile):
    dirname, pyname = os.path.split(os.path.abspath(sys.argv[0]))
    f = (dirname + "/" + fromFile)
    return f

def pullLine(startsWith, fromFilename, outFilename, appendBool, header=None):
    open(outFilename, 'w').close()
    fout=open(outFilename, "a")
    ffrom = open(getPath(fromFilename))
    if header != None:
        fout.write(header)
        fout.write("\n")

    bookingNumber = ""
    for line in ffrom:
        if line.startswith("\"") and not line.startswith("\"ADDRESS:"):
            bookingNumber = re.search(r'[,](\d+)[,]', line).group(0)
            bookingNumber = re.search(r'(\d+)', bookingNumber).group(0)
        if line.startswith(startsWith) and not line.startswith("  , ,"):
            if appendBool:
                newLine = str(bookingNumber) + line
                fout.write(newLine)
            else:
                fout.write(line)
    fout.close()

def cleanFile(fromFile, junk, outFile):
    open(outFile, 'w').close()
    fout=open(outFile, "a")
    ffrom = open(getPath(fromFile))

    for line in ffrom:
        cleaned_line = line

        for item in junk:
            cleaned_line = cleaned_line.replace(item, "")
        cleaned_line = cleaned_line.replace(" \"", "\"")
        cleaned_line = cleaned_line.replace(" ,", ",")
        cleaned_line = cleaned_line.replace(", ", ",")
        fout.write(cleaned_line)
    fout.close()

def releaseClean():
    #create file with only lines starting with "RELEASE DATE: "
    header = "releaseDate,releaseCode,SOID"
    pullLine("RELEASE DATE:", "combined_LayoutB.csv", "releaseDateLines.csv", False, header)
    #clean release file
    # replace some text

    junkText = ["RELEASE DATE: ", "RELEASE CODE:", "SOID:", ",,,,"]
    cleanFile("releaseDateLines.csv", junkText, "huntReleaseClean.csv")

def addlChargesClean():
    open("addlChargesPartialClean.csv", 'w').close()
    fout = open("addlChargesPartialClean.csv", "a")
    #if line begins with " ,Letter", append booking number and write
    header = "bookingNumber,chargeType,charge,court,caseNumber"
    pullLine("  ,", "combined_LayoutB.csv", "booking_addlCharges.csv", True, header)
    #clean
    ffrom = open(getPath("booking_addlCharges.csv"))
    for line in ffrom:
        line2 = re.sub(r'(?!(([^"]*"){2})*[^"]*$),', '.', line)
        line3 = re.sub(r'[,][,]\n', '\n', line2)
        fout.write(line3)
    fout.close()
    cleanFile("addlChargesPartialClean.csv", ["  "], "huntBooking_addlChargesClean.csv")

#combine the layout B files into one
combinefiles("combined_LayoutB.csv")
#make release.clean
releaseClean()
#make booking_addl-charge.clean
addlChargesClean()